import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.Model;


public class Graphics
{
    private Model model;
    private GraphicsContext gc;


    public Graphics(Model model, GraphicsContext gc)
    {
        this.model = model;
        this.gc = gc;
    }


    // insert Sudoku template (created in Model-Class

    public void insertTemplate(int[][] sudokuTemplate)
    {
        gc.setFont(Font.font("Verdana", 10));
        int x = 25;
        int y = 25;

        for (int row = 0; row < 9; row++)
        {
            gc.setFill(Color.BLACK);

            for (int col = 0; col < 9; col++)
            {
                String input = String.valueOf(sudokuTemplate[col][row]);
                if (input.equals("0"))
                {
                    input = "";
                }

                gc.fillText(input, x, y);
                y += 30;
            }
            y = 25;
            x += 30;
        }

    }


    // draw panel with 1 box for each number, with separation lines

    public void drawPanel()

    {
        int xb = 10;
        int yb = 10;

        // draw boxes
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                int w = 30;
                int h = 30;

                if (i == model.getSelectedX() && j == model.getSelectedY())
                {
                    gc.setFill(Color.DARKGRAY);

                } else
                {
                    gc.setFill(Color.LIGHTGRAY);
                }
                gc.fillRect(xb, yb, w, h);
                yb += 30;
            }
            yb = 10;
            xb += 30;

        }

        int xl1 = 40;
        int yl1 = 40;
        int xl2 = 40;
        int yl2 = 40;

        // draw inner lines
        for (int i = 0; i < 8; i++)
        {
            gc.setStroke(Color.DARKGRAY);
            gc.strokeLine(xl1, 10, xl2, 280);
            xl1 += 30;
            xl2 += 30;
        }

        for (int i = 0; i < 8; i++)
        {
            gc.setStroke(Color.DARKGRAY);
            gc.strokeLine(10, yl1, 280, yl2);
            yl1 += 30;
            yl2 += 30;
        }

        // draw outer lines
        gc.setStroke(Color.BLACK);
        gc.strokeLine(100, 10, 100, 280);
        gc.strokeLine(190, 10, 190, 280);
        gc.strokeLine(10, 100, 280, 100);
        gc.strokeLine(10, 190, 280, 190);


        // Sudoku Heading

        gc.setFont(Font.font("Verdana", FontWeight.BOLD, 35));
        gc.setFill(Color.DARKGRAY);
        gc.fillText("SUDOKU", 350, 50);
        gc.setFont(Font.font("Verdana", 12));
        gc.fillText("Each row, column, and box can \ncontain each number (1 to 9) \nexactly once.", 350, 100);


    }

    // Timer graphics
    public void timer()
    {
        gc.clearRect(350, 200, 100, 100);
        gc.setFont(Font.font("Verdana", 12));
        gc.setFill(Color.DARKGRAY);
        gc.fillText("Time: " + model.getTime() / 1000, 350, 250);

    }


    public void incorrectInputMessage()
    {
        if (model.isInputOK())
        {
            gc.clearRect(40, 300, 200, 100);
        } else
        {
            gc.setFont(Font.font("Verdana", 12));
            gc.setFill(Color.GOLDENROD);
            gc.fillText("Incorrect input, please try again!", 40, 350);
        }

    }

}

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import model.Model;

public class Main extends Application
{

    @Override
    public void start(Stage stage) throws Exception
    {
        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);

        Group group = new Group();
        group.getChildren().add(canvas);

        Scene scene = new Scene(group);

        stage.setScene(scene);
        stage.show();

        GraphicsContext gc = canvas.getGraphicsContext2D();
        Model model = new Model();
        Graphics graphics = new Graphics(model, gc);
        graphics.drawPanel();
        graphics.insertTemplate(model.getSudoku1());

        Timer timer = new Timer(model, graphics);
        timer.start();

        // User Input
        InputHandler inputHandler = new InputHandler(model);
        scene.setOnKeyReleased(event -> inputHandler.onKeyPressed(event.getCode()));
        canvas.setOnMouseClicked(event -> inputHandler.onClick(event.getX(), event.getY()));

    }

}

package model;

public class Model
{
    public static final int WIDTH = 600;
    public static final int HEIGHT = 400;

    private int selectedX = 8;
    private int selectedY = 8;

    boolean inputOK = true;
    long time = 0;


    private int[][] sudoku1 =
            {{0, 1, 2, 0, 0, 0, 5, 7, 0},
                    {6, 0, 0, 5, 0, 1, 0, 0, 4},
                    {4, 0, 0, 0, 2, 0, 0, 0, 8},
                    {0, 2, 0, 0, 1, 0, 0, 5, 0},
                    {0, 0, 4, 9, 0, 7, 8, 0, 0},
                    {0, 7, 0, 0, 8, 0, 0, 1, 0},
                    {7, 0, 0, 0, 9, 0, 0, 0, 5},
                    {5, 0, 0, 4, 0, 8, 0, 0, 6},
                    {0, 3, 8, 0, 0, 0, 9, 4, 0},
            };


    public int[][] getSudoku1()
    {
        return sudoku1;
    }

    public int getSelectedX()
    {
        return selectedX;
    }

    public int getSelectedY()
    {
        return selectedY;
    }

    public void setSelectedField(int a, int b)
    {
        this.selectedX = a;
        this.selectedY = b;
    }

    public boolean isInputOK()
    {
        return inputOK;
    }


    // Solve

    public void solve(int input)
    {
        checkInput(selectedY, selectedX, input);

        if (inputOK)
        {
            sudoku1[selectedY][selectedX] = input;
        } else
        {
            sudoku1[selectedY][selectedX] = 0;
        }

    }


    // check if input is already defined in row,col or box

    public void checkInput(int row, int col, int input)
    {
        if (isInRow(row, input) || isInCol(col, input) || isInBox(row, col, input))
        {
            inputOK = false;
        } else
        {
            inputOK = true;
        }

    }


    // if input is already in row, true is returned
    private boolean isInRow(int row, int input)
    {
        for (int i = 0; i < 9; i++)
        {
            if (sudoku1[row][i] == input)
            {
                return true;
            }
        }
        return false;
    }


    // if input is already in column, true is returned
    private boolean isInCol(int col, int input)
    {
        for (int i = 0; i < 9; i++)
        {
            if (sudoku1[i][col] == input)
            {
                return true;
            }
        }
        return false;
    }


    // if input is already in box, true is returned
    private boolean isInBox(int row, int col, int input)
    {
        int r = row - row % 3;
        int c = col - col % 3;

        for (int i = r; i < r + 3; i++)
        {
            for (int j = c; j < c + 3; j++)
            {
                if (sudoku1[i][j] == input)
                {
                    return true;
                }
            }
        }
        return false;
    }

    // Timer model
    public void timer(long elapsedTime)
    {
        time += elapsedTime;
    }

    public long getTime()
    {
        return time;
    }
}

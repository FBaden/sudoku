import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler
{
    private Model model;
    private int inputNumber;


    public InputHandler(Model model)
    {
        this.model = model;
    }


    public void onKeyPressed(KeyCode key)
    {

        if (key == KeyCode.DIGIT0)
        {
            inputNumber = 0;
        } else if (key == KeyCode.DIGIT1)
        {
            inputNumber = 1;
        } else if (key == KeyCode.DIGIT2)
        {
            inputNumber = 2;
        } else if (key == KeyCode.DIGIT3)
        {
            inputNumber = 3;
        } else if (key == KeyCode.DIGIT4)
        {
            inputNumber = 4;
        } else if (key == KeyCode.DIGIT5)
        {
            inputNumber = 5;
        } else if (key == KeyCode.DIGIT6)
        {
            inputNumber = 6;
        } else if (key == KeyCode.DIGIT7)
        {
            inputNumber = 7;
        } else if (key == KeyCode.DIGIT8)
        {
            inputNumber = 8;
        } else if (key == KeyCode.DIGIT9)
        {
            inputNumber = 9;
        }

        model.solve(inputNumber);

    }

    public void onClick(double x, double y)
    {
        int a = (int) ((x - 10) / 30);
        int b = (int) ((y - 10) / 30);
        model.setSelectedField(a, b);

    }


}

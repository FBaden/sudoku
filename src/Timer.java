import javafx.animation.AnimationTimer;
import model.Model;

public class Timer extends AnimationTimer
{
    private long previousTime = -1;
    private Model model;
    private Graphics graphics;


    public Timer(Model model, Graphics graphics)
    {
        this.model = model;
        this.graphics = graphics;
    }

    @Override
    public void handle(long nowNanoS)
    {
        long nowMilliS = nowNanoS / 1000000;
        long elapsedTime;

        if (previousTime == -1)
        {
            elapsedTime = 0;
        } else
        {
            elapsedTime = nowMilliS - previousTime;
        }
        previousTime = nowMilliS;

        graphics.drawPanel();
        graphics.insertTemplate(model.getSudoku1());
        graphics.incorrectInputMessage();

        model.timer(elapsedTime);
        graphics.timer();

    }
}
